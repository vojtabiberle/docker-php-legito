# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:0.11
MAINTAINER Vojtěch Biberle <vojtech.biberle@gmail.com>

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y \
        php7.1 \
        php7.1-cgi \
        php7.1-curl \
        php7.1-gd \
        php7.1-gmp \
        php7.1-json \
        php7.1-ldap \
        php7.1-mysql \
        php7.1-odbc \
        php7.1-opcache \
        php7.1-pgsql \
        php7.1-pspell \
        php7.1-readline \
        php7.1-recode \
        php7.1-sqlite3 \
        php7.1-tidy \
        php7.1-xml \
        php7.1-xmlrpc \
        php7.1-bcmath \
        php7.1-bz2 \
        php7.1-enchant \
        php7.1-fpm \
        php7.1-imap \
        php7.1-interbase \
        php7.1-intl \
        php7.1-mbstring \
        php7.1-mcrypt \
        php7.1-phpdbg \
        php7.1-soap \
        php7.1-sybase \
        php7.1-xsl \
        php7.1-zip \
        php7.1-ssh2 \
        php-memcached \
        php-xdebug \
        php-pear \
        git \
        unzip \
	libreoffice

RUN update-alternatives --set php /usr/bin/php7.1

RUN apt-get install -y php7.1-dev libsodium-dev && \
    pecl channel-update pecl.php.net && \
    pecl install -f libsodium-1.0.7 && \
    apt-get purge -y php7.1-dev php7.2 php7.2* && \
    apt-get autoremove -y

ADD root /

ENV WEBAPP_DIR /var/www/website
ENV WEBAPP_USER webapp
ENV WEBAPP_GROUP webapp
ENV WEBAPP_GID 1000
ENV WEBAPP_UID 1000
ENV WEBAPP_PHP_FPM_LISTEN 9000

ENV COMPOSER_HOME /composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

WORKDIR $WEBAPP_DIR
VOLUME [$WEBAPP_DIR]
RUN phpenmod essentials
RUN phpenmod libsodium
RUN groupadd -g $WEBAPP_GID $WEBAPP_GROUP && \
    useradd -d $WEBAPP_DIR -g $WEBAPP_GID -u $WEBAPP_UID $WEBAPP_USER && \
    chown $WEBAPP_USER:$WEBAPP_GROUP $WEBAPP_DIR && \
    chown $WEBAPP_USER:$WEBAPP_GROUP /run/php && \
    chown $WEBAPP_USER:$WEBAPP_GROUP /composer
RUN sed -i "s/^;daemonize = yes/daemonize = no/g" /etc/php/7.1/fpm/php-fpm.conf
RUN sed -i "s/^user = www-data/user = $WEBAPP_USER/g" /etc/php/7.1/fpm/pool.d/www.conf && \
    sed -i "s/^group = www-data/group = $WEBAPP_GROUP/g" /etc/php/7.1/fpm/pool.d/www.conf && \
    sed -i "s/^listen.owner = www-data/listen.owner = $WEBAPP_GROUP/g" /etc/php/7.1/fpm/pool.d/www.conf && \
    sed -i "s/^listen.group = www-data/listen.group = $WEBAPP_GROUP/g" /etc/php/7.1/fpm/pool.d/www.conf && \
    sed -i "s@^listen = /run/php/php7.1-fpm.sock@listen = $WEBAPP_PHP_FPM_LISTEN@g" /etc/php/7.1/fpm/pool.d/www.conf && \
    sed -i "s@^;chdir = /var/www@chdir = $WEBAPP_DIR@g" /etc/php/7.1/fpm/pool.d/www.conf && \
    sed -i "s/^;clear_env = no/clear_env = no/g" /etc/php/7.1/fpm/pool.d/www.conf

EXPOSE 9000

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
