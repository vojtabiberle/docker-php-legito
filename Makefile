MAINTAINER_NS=vojtabiberle
NAME=docker-php-legito
VERSION=7.1
REGISTRY=registry.gitlab.com

files = Dockerfile root/etc/php/7.1/mods-available/essentials.ini root/etc/service/php-fpm/run

build: $(files)
	docker build -t ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION} .

run:
	docker run --rm -i -t ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION} /sbin/my_init -- bash -l

push:
	docker push ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION}
